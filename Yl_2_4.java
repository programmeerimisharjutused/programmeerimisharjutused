class Yl_2_4 {
  // Kolmas number alumisest otsast.
  // n % 1000 viskab ara kõik sajalistest ylespoole jaavad jargud.
  // Kui see tulemus nyyd 100-ga (taisarvuliselt) labi jagada, jaab alles algse arvu n kolmas number alumisest otsast.
  // Naiteks 123456 korral 123456 % 1000 = 456 ja 456 / 100 = 4.
  static int kolmas (int n) {
    return (n % 1000) / 100;
  }
  static void testKolmas (int n) {
    System.out.println ("Arvu " + n + " kolmas number on " + kolmas (n) + ".");
  }
  public static void main (String [] args) {
    testKolmas (0);
    testKolmas (1);
    testKolmas (12);
    testKolmas (123);
    testKolmas (1234);
    testKolmas (12345);
    testKolmas (123456);
  }
}