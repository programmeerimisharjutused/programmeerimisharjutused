class arg2 {
  Tipp t;
  String str0, str1;
  
  arg2(Tipp t, String str0, String str1) {
    this.t = t;
    this.str0 = str0;
    this.str1 = str1;
  }
}

class k40test {
  
  static int juhuarv(int a,int b){
    return (int)(Math.random()*(b-a+1)+a);
  }

  /**
   * Juhusliku puu koostamine.
   * @param korgus Koostatava puu kõrgus.
   * @param a Puusse salvestatavate väärtuste alampiir.
   * @param b Puusse salvestatavate väärtuste ülempiir.
   * return Parameetrite poolt seatud tingimustele vastav juhuslike väärtustega puu, kus kõrvalharud juhusliku kõrgusega.
   */
  static Tipp juhupuu(int korgus, int a, int b) {
  // tagastab antud kõrgusega juhusliku puu
  // tipukirjeteks arvud lõigult [a;b]
    if (korgus == 0)
      return null;
    Tipp uus = new Tipp();
    uus.x = juhuarv(a, b);
    if (juhuarv(0, 1) == 0) {
      uus.v = juhupuu(korgus - 1, a, b);
      uus.p = juhupuu(juhuarv(0, korgus - 1), a, b);
    } else {
      uus.v = juhupuu(juhuarv(0, korgus - 1), a, b);
      uus.p = juhupuu(korgus - 1, a, b);
    }
    return uus;
  }
  
  static int korgus(Tipp t) {
    if (t == null)
      return 0;
    return 1 + Math.max(korgus(t.v), korgus(t.p));
  }
  
  /**
   * Puu printimise abimeetod: pildi koostamine sümbolite tabelina.
   * @param t Prinditav puu.
   * @param laius Horisontaalmõõde (tähemärkides), millele pilt peab mahtuma.
   * @param horalgus Pildi vasaku ääre horisontaalkoordinaat tabelis.
   * @param veralgus Pildi ülemise ääre vertikaalkoordinaat tabelis.
   * @param eelm Juurtipu ülemuse horisontaalkoordinaat (-1, kui pole).
   * @param tul Tabel pildi salvestamiseks.
   * @return 
   */
  static void printPabi(Tipp t, int laius, int horalgus, int veralgus, int eelm, char[][] tul) {
    int kesk = horalgus + laius / 2;
    if (t != null) {
      if (eelm >= 0) {
        tul[veralgus][eelm] = '|';
        int a = eelm;
        int b = kesk;
        if (eelm > kesk) {
          int z = a;
          a = b;
          b = z;
        }
        for (int i=a; i<=b; i++)
          tul[veralgus+1][i] = '-';
      }
      tul[veralgus+2][kesk] = '|';
      for (int i=0; i<t.info.length(); i++)
        tul[veralgus+3][kesk-t.info.length()/2+i] = t.info.charAt(i);
      printPabi(t.v, (laius - 4) / 2, horalgus, veralgus + 4, kesk, tul);
      printPabi(t.p, (laius - 4) / 2, kesk + 2, veralgus + 4, kesk, tul);
    }
  }
  
  // Prindib puu. NB! Töötab kuni 5-täheliste infokirjetega.
  static void printP(Tipp t) {
    if (t == null)
      System.out.println("[tühi]");
    else {
      int korgus = korgus(t);
      int laius = (int)(Math.pow(2,korgus)-1);
      char[][] pilt = new char[4*korgus][4*laius+1];
      for (int i=0; i<pilt.length; i++)
        for (int j=0; j<pilt[0].length; j++)
          pilt[i][j] = ' ';
      printPabi(t, pilt[0].length, 0, 0, -1, pilt);
      for (int i=2; i<pilt.length; i++) 
        System.out.println(new String(pilt[i]));
    }
  }
  
  static void tootle(Throwable e) {
    System.out.println("Tekkis erind:");
    e.printStackTrace();
  }
  
  // Testmeetodid võtavad samasugused argumendid nagu vastavad põhimeetodid.
  
  // Ül 1 testmeetod
  static void ul1test(Tipp t) {
    System.out.println("Antud puu:");
    printP(t);
    System.out.println();
    System.out.print("Meetodi tagastusväärtus: ");
    try {
      System.out.println(k40.ul1(t));
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
    try {
      System.out.println("Antud puu peale meetodi täitmist:");
      printP(t);
      System.out.println();
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
  }
  
  // Ül 2 testmeetod
  static void ul2test(Tipp t, String str0, String str1) {
    System.out.println("Antud sõned: \"" + str0 + "\" ja \"" + str1 + "\"");
    System.out.println("Antud puu:");
    printP(t);
    System.out.println();
    System.out.println("Meetodi tagastusväärtus:");
    try {
      printP(k40.ul2(t, str0, str1));
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
    try {
      System.out.println("Antud puu peale meetodi täitmist:");
      printP(t);
      System.out.println();
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
  }
  
  // Abimeetod; kirjutab antud teksti paksus kirjas punaselt
  private static String esil(String tekst) {
    return "\u001B[31;1m" + tekst + "\u001B[0m";
  }
  
  // Peameetod
  public static void main(String[] args) {
    String joon = "-----------------------------------------";
    System.out.println(joon + "\nMeetodi " + esil("ul1") + " testid\n" + joon);
    System.out.println();
    Tipp[] arg1s = {
                     new Tipp("A",new Tipp("B",null,new Tipp("D",null,new Tipp("G",null,null))),new Tipp("C",new Tipp("E",new Tipp("H",null,null),null),new Tipp("F",new Tipp("I",null,null),new Tipp("J",null,null))))
                     // Siin lisa/asenda oma testandmeid samal viisil
                   };
    for (int i=0; i<arg1s.length; i++)
      ul1test(arg1s[i]);
    System.out.println();
    System.out.println(joon + "\nMeetodi " + esil("ul2") + " testid\n" + joon);
    System.out.println();
    arg2[] arg2s = {
                     new arg2(new Tipp("na",new Tipp("na",new Tipp("na",null,null),null),new Tipp("na",new Tipp("na",null,new Tipp("na",null,null)),new Tipp("na",null,null))),"mu","ka")
                     // Siin lisa/asenda oma testandmeid samal viisil
                   };
    for (int i=0; i<arg2s.length; i++)
      ul2test(arg2s[i].t,arg2s[i].str0,arg2s[i].str1);
    System.out.println();
    System.out.println(joon);
    System.out.println("Töö autor: " + k40.nimi);
    System.out.println();
  }
    
}

