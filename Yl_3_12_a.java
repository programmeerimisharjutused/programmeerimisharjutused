class Yl_3_12_a {
  // Meetod joonistab ruudu suurusega n korda n.
  static void ruut (int n) {
    // Alustuseks reavahetus, et uus kujund eelmisest eraldada.
    System.out.print ("\n");
    // Joonistame kujundi jarjest ridade ja veergude kaupa. i on rea ja j on veeru indeks.
    // Naiteks 5 korda 5 ruudu korral on indeksid (i, j) jargmised:
    // (0, 0) (0, 1) (0, 2) (0, 3) (0, 4)
    // (1, 0) (1, 1) (1, 2) (1, 3) (1, 4)
    // (2, 0) (2, 1) (2, 2) (2, 3) (2, 4)
    // (3, 0) (3, 1) (3, 2) (3, 3) (3, 4)
    // (4, 0) (4, 1) (4, 2) (4, 3) (4, 4)
    for (int i = 0; i < n; i ++) {
      for (int j = 0; j < n; j ++) {
        // Kui i == 0, siis tegu on ylemise servaga.
        // Kui i == n - 1, siis tegu on alumise servaga.
        // Kui j == 0, siis tegu on vasaku servaga.
        // Kui j == n - 1, siis tegu on parema servaga.
        // Kui ykskoik milline neist tingimustest on taidetud ehk tegu on servaruudukesega, siis taidame.
        // Vastasel juhul on tegu ruudukesega ruudu keskelt, mille jatame tyhjaks.
        if (i == 0 || i == n - 1 || j == 0 || j == n - 1) {
          System.out.print ("#");
        } else {
          System.out.print (" ");
        }
      }
      // Iga rea lopus reavahetus.
      System.out.print ("\n");
    }
  }
  public static void main (String [] args) {
    // Joonistame testiks ruudud suurusega 1 kuni 5.
    for (int i = 1; i <= 5; i ++) {
      ruut (i);
    }
  }
}