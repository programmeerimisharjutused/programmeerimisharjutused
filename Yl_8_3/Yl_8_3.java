class Yl_8_3 {
  // tulemus on seni kogutud tulemus.
  // StringBuilder-is s on tahed, mida me veel ei ole kasutanud ja voime kasutada.
  // k ytleb, mitu tahte veel on tarvis tulemusele lisada.
  static void permutatsioonid1 (String tulemus, StringBuilder s, int k) {
    // Kui tulemusele ei ole enam tarvis yhtegi tahte lisada, siis ta on valmis ja trykime ta valja.
    if (k == 0) {
      System.out.println (tulemus);
    } else {
      // Kui tulemus ei ole veel valmis, siis hargneme mitmeks.
      // Tulemuse iga voimaliku jargneva tahe kohta (mille votame StringBuilder-ist s) on yks rekursiooni haru.
      for (int i = 0; i < s . length (); ++ i) {
        // Lisame valitud tahe tulemuse loppu.
        // Eemaldame kasutatud tahe kasutamata tahtede nimekirjast.
        // NB! new StringBuilder on vajalik, et yks rekursiooni haru ei rikuks ara teistele harudele vajalikke andmeid!
        // Tahtede arv, mille peame veel tulemusele lisama, vaheneb yhe vorra.
        permutatsioonid1 (tulemus + s . charAt (i), new StringBuilder (s) . deleteCharAt (i), k - 1);
      }
    }
  }
  // Eeldame, et sones s puuduvad korduvad tahed.
  static void permutatsioonid (String s, int k) {
    // Alguses on tulemussone tyhi.
    permutatsioonid1 ("", new StringBuilder (s), k);
  }
  public static void main (String [] args) {
    permutatsioonid ("karu", 3);
    // See test naitab, et kui sones on korduvaid tahti, siis annab meetod permutatsioonid korduvaid tulemusi.
    permutatsioonid ("kass", 2);
  }
}