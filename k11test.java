class k11test {
  
  static void tootle(Throwable e) {
    System.out.println("Tekkis erind:");
    e.printStackTrace();
  }
  
  static void printR(double[] mas) {
    for (int i=0; i<mas.length; i++)
      System.out.print(mas[i] + " ");
  }
  
  static void printJ(int[] mas) {
    for (int i=0; i<mas.length; i++)
      System.out.print(mas[i] + " ");
  }
  
  static void printM(int[][] tab) {
    for (int i=0; i<tab.length; i++) {
      printJ(tab[i]);
      System.out.println();
    }
  }
  
  // Testmeetodid võtavad samasugused argumendid nagu ülesande meetodid
  
  // Ül 1 testmeetod
  static void ul1test(int n) {
    try {
      System.out.println("Meetodi väljund parameetril " + n + ":");
      System.out.println();
      k11.ul1(n);
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
  }
  
  // Ül 2 testmeetod
  static void ul2test(int[] a) {
    try {
      System.out.print("Massiivil elementidega ");
      printJ(a);
      System.out.print("tagastatakse ");
      System.out.println(k11.ul2(a) + ".");
      System.out.print("Algses massiivis pärast meetodi töö lõppu on elemendid ");
      printJ(a);
      System.out.println();
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
  }
  
  // Ül 3 testmeetod
  static void ul3test(int[] a) {
    try {
      System.out.println("Massiivil elementidega ");
      System.out.println();
      printJ(a);
      System.out.println();
      System.out.println();
      System.out.println("tagastatakse maatriks");
      System.out.println();
      printM(k11.ul3(a));
      System.out.println();
      System.out.println();
      System.out.println("Algses massiivis pärast meetodi töö lõppu on elemendid ");
      printJ(a);
      System.out.println();
      System.out.println();
      System.out.println("--");
      System.out.println();
    } catch (Throwable e) {
      tootle(e);
    }
  }
  
  // Abimeetod; kirjutab antud teksti paksus kirjas punaselt
  private static String esil(String tekst) {
    return "\u001B[31;1m" + tekst + "\u001B[0m";
  }
  
  // Peameetod
  public static void main(String[] args) {
    String joon = "--------------------------------------------";
    System.out.println(joon + "\nMeetodi " + esil("ul1") + " testid\n" + joon);
    System.out.println();
    int[] arg1s = {
                    1
                  , 2
                  , 4
                    // Siin lisa/asenda oma testandmeid samal viisil
                  };
    for (int i=0; i<arg1s.length; i++)
      ul1test(arg1s[i]);
    System.out.println();

    System.out.println(joon + "\nMeetodi " + esil("ul2") + " testid\n" + joon);
    System.out.println();
    int[][] arg2s = {
                      new int[]{1,2,7,8}
                    , new int[]{1,3,5,0}
                    // Siin lisa/asenda oma testandmeid samal viisil
                  };
    for (int i=0; i<arg2s.length; i++)
      ul2test(arg2s[i]);
    System.out.println();

    System.out.println(joon + "\nMeetodi " + esil("ul3") + " testid\n" + joon);
    System.out.println();
    int[][] arg3s = {
                      new int[]{5,3,1,3,5,6}
                    , new int[]{1,2,3,4,5,6,7}
                    , new int[]{9,8,7,6,5,4,3,2,1,0}
                    , new int[]{6}
                    // Siin lisa/asenda oma testandmeid samal viisil
                  };
    for (int i=0; i<arg3s.length; i++)
      ul3test(arg3s[i]);
    System.out.println();
  }
  
}
