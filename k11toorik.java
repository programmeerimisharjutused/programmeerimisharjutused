// java.util.Arrays on imporditud sisseehitatud sorteerimise jaoks.
// Ylesannet 2 saab lahendada ka ilma sorteerimiseta.
import java.util.Arrays;
class k11 {
  static String nimi = "Liisi Kerik";
  static void ul1 (int n) {
    // Kolmnurk joonistatakse n korda n ruudustikku.
    // i on rea ja j on veeru indeks.
    for (int i = 0; i < n; ++ i) {
      for (int j = 0; j < n; ++ j) {
        // i == 0 tahendab, et tegu on esimese reaga (kolmnurga ylemine kaatet).
        // i == j tahendab, et tegu on ruudustiku peadiagonaaliga (kolmnurga hypotenuus).
        // j == n - 1 tahendab, et tegu on viimase veeruga (kolmnurga parempoolne kaatet).
        // Kui ruudustiku vastav ruuduke on kolmnurga servaruuduke, siis taidame selle.
        if (i == 0 || i == j || j == n - 1) {
          System.out.print ('#');
        // Vastasel juhul trykime tyhiku.
        } else {
          System.out.print (' ');
        }
      }
      // Rea lopus reavahetus.
      System.out.print ("\n");
    }
  }
  // Ylesande 2 lihtsam, aga aeglasem lahendus. Sellest kontrolltoos tegelikult piisas.
  static boolean ul21 (int [] a) {
    int n = a . length;
    // Iga jarjestikuste elementide paari a [i] ja a [i + 1] jaoks leiame nende summa a [i] + a [i + 1].
    // Paneme tahele, et indeks i peab pidama jaama n - 2 mitte n - 1 juures.
    // Vastasel juhul arvutaksime viimases iteratsioonis a [n - 1] + a [n] ja katse a [n] leida paneks programmi kokku jooksma.
    for (int i = 0; i < n - 1; ++ i) {
      // Vordleme kahe jarjestikuse elemendi summat massiivi koigi elementidega. Kui leiame vordse, tagastame true.
      for (int j = 0; j < n; ++ j) {
        if (a [i] + a [i + 1] == a [j]) {
          return true;
        }
      }
    }
    // Kui me ei leidnud sellist kahte jarjestikust elementi, mis vorduks mone massiivi elemendiga, siis tagastame false.
    return false;
  }
  // Boonus - ylesande 2 keerulisem, aga efektiivsem lahendus, mis kasutab sisseehitatud sorteerimist.
  static boolean ul2 (int [] a) {
    int n = a . length;
    // Algoritm jookseks kokku, kui n = 0, sest hiljem yritame luua massiivi pikkusega n - 1. Seega tagastame kohe false.
    // Juhul n = 1 voime ka kohe tagastada false, sest yhtegi jarjestikuste elementide paari yheelemendilises massiivis ei ole.
    if (n < 2) {
      return false;
    }
    // Kopeerime sisendmassiivi, et seda sortimisega mitte ara rikkuda. Sorteerime.
    int [] a1 = a . clone ();
    Arrays . sort (a1);
    // Loome yhe vorra lyhema abimassiivi, milles hoida jarjestikuste elementide summat.
    // Naiteks kui sisendmassiiv on {1, 2, 3, 4}, siis summade massiiv tuleb {3, 5, 7}.
    int [] summad = new int [n - 1];
    for (int i = 0; i < n - 1; ++ i) {
      // Siin on tahtis kasutada algset massiivi a mitte selle sorteeritud koopiat a1.
      // Massiivi a1 elemendid on ju vales jarjekorras, mis tahendab, et saaksime valede paaride summad.
      summad [i] = a [i] + a [i + 1];
    }
    // Sorteerime ka summade massiivi.
    Arrays . sort (summad);
    // Kahest sorteerimata massiivist saaksime leida vordseid elemente topelttsykliga.
    // Kahe sorteeritud massiivi korral piisab yhekordsest tsyklist.
    // Kuna sisseehitatud sorteerimismeetod on kiirem kui topelttsykel, on tegu olulise voiduga efektiivsuses.
    // Alustame molema massiivi esimesest elemendist.
    int i = 0;
    int j = 0;
    // Itereerime seni, kuni oleme joudnud kas esimese voi teise massiivi loppu, voi kuni leiame kaks vordset elementi.
    // Igas iteratsioonis liigume yhe vorra edasi selles massiivis, mille vastav element on vaiksem.
    while (i < n && j < n - 1) {
      // Kui esimese massiivi vastav element on vaiksem, siis suurendame indeksit i.
      if (a1 [i] < summad [j]) {
        ++ i;
      // Kui massiivide vastavad elemendid on vordsed, tagastame true.
      } else if (a1 [i] == summad [j]) {
        return true;
      // Kui teise massiivi vastav element on vaiksem, siis suurendame indeksit j.
      } else if (a1 [i] > summad [j]) {
        ++ j;
      }
    }
    // Kui vordseid elemente ei leidnud, tagastame false.
    return false;
  }
  static int [] [] ul3 (int [] a) {
    // l on sisendmassiivi elementide arv.
    int l = a . length;
    // Kui sisendmassiiv on pikkusega 0, tagastame kohe 0 korda 0 maatriksi.
    // Kui me seda ei tee, siis voib juhtuda, et m = 0 ja tulemus [m - 1] tekitab negatiivse indeksiga indekseerimise.
    if (l == 0) {
      return new int [0] [0];
    }
    // Selleks, et mahutada n elementi m korda m massiivi, peab kehtima, et m ^ 2 >= n ehk m >= ceil (ruutjuur n).
    // ceil ymardab yles.
    // (int) teisendab komaga arvu taisarvuks.
    // n on tulemusmassiivi ridade ja veergude arv. dn on seesama ujuvkomaarvuna.
    double dn = Math.ceil (Math.sqrt (l));
    int n = (int) dn;
    // m on tulemusmaatriksi tegelike (st elemente sisaldavate, mitte-0-pikkusega) ridade arv.
    // Selle leiame jagades elementide arvu rea pikkusega ja ümardades üles.
    int m = (int) (Math.ceil (l / dn));
    // Loome vajaliku ridade arvuga tulemusmassiivi. Viimased ebavajalikud read saavad olema 0-elemendilised massiivid.
    int [] [] tulemus = new int [n] [];
    // Täidame tulemusmassiivi vajaliku pikkusega ridadega.
    // r tähistab rea numbrit.
    // Alguses on read taispikkusega n.
    for (int r = 0; r < m - 1; ++ r) {
      tulemus [r] = new int [n];
    }
    // Viimasesse elemente sisaldavasse ritta lahevad koik elemendid, mis m - 1 taielikku (pikkusega n) ritta ei mahtunud.
    tulemus [m - 1] = new int [l - (m - 1) * n];
    // Yleliigsed read on 0-elemendilised.
    for (int r = m; r < n; ++ r) {
      tulemus [r] = new int [0];
    }
    // Paigutame kahemootmelisse tulemusmassiivi jarjest sisendmassiivi elemendid.
    for (int i = 0; i < l; ++ i) {
      // Iga tulemusmassiivi rida (peale voib-olla viimase) sisaldab n elementi.
      // Rea indeksi saame, jagades indeksi i tulemusmassiivi laiusega.
      // i / n ytleb, mitu rida on juba ara tarvitatud ja mis indeksiga real me jarelikult oleme.
      // Veeru indeksi saame, jagades indeksi i tulemusmassiivi laiusega.
      // i % n ytleb, kui kaugel me praeguses reas oleme.
      tulemus [i / n] [i % n] = a [i];
    }
    return tulemus;
  }
}