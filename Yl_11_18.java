class Yl_11_18 {
  static String pikimaTeeInfo1 (Tipp t) {
    if (t . v == null && t . p == null) {
      t . x = 0;
      return t . info;
    }
    if (t . v == null) {
      String infop = pikimaTeeInfo1 (t . p);
      t . x = t . p . x + 1;
      return t . info + "," + infop;
    }
    if (t . p == null) {
      String infov = pikimaTeeInfo1 (t . v);
      t . x = t . v . x + 1;
      return t . info + "," + infov;
    }
    String infov = pikimaTeeInfo1 (t . v);
    String infop = pikimaTeeInfo1 (t . p);
    t . x = Math.max (t . v . x, t . p . x) + 1;
    return t . info + "," + (t . v . x < t . p . x ? infop : infov);
  }
  static String pikimaTeeInfo (Tipp t) {
    if (t == null) {
      return "";
    }
    return pikimaTeeInfo1 (t);
  }
  static void test (Tipp t) {
    System.out.println ("Puu:");
    System.out.println (Tipp . toString (t));
    System.out.println ("Info:");
    System.out.println (pikimaTeeInfo (t));
  }
  public static void main (String [] args) {
    test (
      new Tipp (
        "J",
        new Tipp (
          "E",
          new Tipp ("B", new Tipp ("A", null, null), new Tipp ("D", new Tipp ("C", null, null), null)),
          new Tipp ("H", new Tipp ("G", new Tipp ("F", null, null), null), new Tipp ("I", null, null))),
        new Tipp ("K", null, new Tipp ("L", null, null))));
  }
}