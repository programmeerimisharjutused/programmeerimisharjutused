class Yl_9_4_c {
  // Analyysime, mitu korrutamistehet teeb ylesandes antud astendamisfunktsioon.
  // Astme arvutamise kulg ei soltu astendatavast a vaid ainult astendajast n.
  // Naiteks 2 ^ 4 ja 3 ^ 4 arvutatakse tapselt samamoodi.
  // Seega korrutamiste arvu leidmiseks on meil tarvis teada ainult astendajat n.
  static int korrutamisi (int n) {
    // Kui astendaja on null, siis ei tehta yhtegi korrutamist.
    if (n == 0) {
      return 0;
    }
    // Kui astendaja on nullist suurem paarisarv, siis astme arvutus toimub nii: a ^ n = a ^ (n / 2) * a ^ (n / 2).
    // Leiame, mitu korrutamist tehakse a ^ (n / 2) arvutamiseks.
    // Korrutame selle kahega, sest a ^ (n / 2) arvutatakse kaks korda.
    // Siis liidame yhe, sest a ^ (n / 2) tulemused tuleb veel omavahel korrutada.
    if (n % 2 == 0) {
      return (1 + 2 * korrutamisi (n / 2));
    }
    // Kui astendaja on nullist suurem paaritu arv, siis astme arvutus toimub nii: a ^ n = a ^ (n / 2) * a ^ (n / 2) * a.
    // See on praktiliselt identne paarisarvulise juhtumiga, selle vahega, et toimub yks lisakorrutamine (* a).
    return (2 + 2 * korrutamisi (n / 2));
  }
  public static void main (String [] args) {
    // Leiame, mitu korrutamistehet teeb ylesandes antud astendamisfunktsioon astmete 0 .. 31 arvutamiseks.
    // Tulemustes markame huvitavat seaduspara, et korrutamiste arv astme n arvutamisel jaab soltuvalt n-st vahemikku [2n, 3n).
    // Ylesandes antud astendamisfunktsioon on jarelikult ysna ebaefektiivne.
    // (Eeldusel, et Java kompilaator ei tee mingit kavalat optimisatsooni arvutades a ^ (n / 2) kahe asemel ainult yhe korra.)
    // Kui salvestada a ^ (n / 2) abimuutujasse (nagu ylesandes 9.4.d), saaksime lineaarse keerukuse asemel logaritmilise.
    // (Naiteks n = 16 korral 6 korrutamist, n = 32 korral 7 korrutamist, n = 64 korral 8 korrutamist, ...)
    // Oige teostuse korral kui n kasvab 2 korda, siis korrutamiste arv kasvab vaid 1 vorra.
    for (int i = 0; i < 32; ++ i) {
      System.out.println ("Astme " + i + " valja arvutamiseks on tarvis " + korrutamisi (i) + " korrutamist.");
    }
  }
}