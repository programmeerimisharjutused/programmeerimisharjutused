// Kaks erinevat implementatsiooni meetodist, mis tagastab lause sonad, milles on vahemalt 3 jarjestikust yhesugust tahte.
import java.util.ArrayList;
import java.util.List;
class Yl_6_4 {
  // Meetod, mis ytleb, kas sonas leidub 3 yhesugust tahte jarjest.
  static boolean kolmYhesugust (String s) {
    // Itereerime yle sona tahtede, et kontrollida, kas leiame 3 yhesugust jarjest.
    // NB - viimane indeks on length - 2, sest muidu juhtuks lopus liiga suur indeks!
    for (int i = 0; i < s . length () - 2; ++ i) {
      // Kontrollime, kas tahed indeksil i, i + 1 ja i + 2 on koik samad.
      if (s . charAt (i) == s . charAt (i + 1) && s . charAt (i) == s . charAt (i + 2)) {
        // Kui on, siis sona rahuldab ylesande tingimusi.
        return true;
      }
    }
    // Kui ei leidnud 3 yhesugust tahte jarjest, siis sona ei rahulda ylesande tingimusi.
    return false;
  }
  // Esimene implementatsioon - massiiviga.
  static String [] kolmYhesugustMassiiviga (String s) {
    // Jagame lause sonadeks.
    String [] sonad = s . split (" ");
    int n = sonad . length;
    // Toevaartuste massiiv, milles margime hiljem true-ks need indeksid, kus on vahemalt 3 jarjestikuse yhesuguse tahega sonad.
    // Alguses on vaikevaartusena koik selle massiivi elemendid false.
    boolean [] sonasKolmYhesugust = new boolean [n];
    // Loendur, mis ytleb, mitu tingimustele vastavat sona me leidsime.
    // Hiljem aitab see meil tekitada tulemuse tagastamiseks tapselt oige suurusega massiiv.
    int sobivaidSonu = 0;
    // Itereerime yle lause sonade.
    for (int i = 0; i < n; ++ i) {
      // Kontrollime, kas leiame 3 yhesugust tahte jarjest.
      if (kolmYhesugust (sonad [i])) {
        // Kui on, siis sona indeksiga i rahuldab ylesande tingimusi.
        sonasKolmYhesugust [i] = true;
        // Suurendame tulevase valjundmassiivi pikkust yhe vorra.
        ++ sobivaidSonu;
      }
    }
    // Tulemusmassiiv on tapselt oige pikkusega, et mahutada koik sobivad sonad.
    String [] tulemus = new String [sobivaidSonu];
    // Kirjutame koik tingimustele vastavad sonad tulemusmassiivi.
    // i on indeks, mis ytleb, kuhu tulemusmassiivis parajasti kirjutada.
    // Suurendame i-d parast seda, kui oleme veel yhe tulemusmassiivi indeksi ara taitnud.
    int i = 0;
    // Itereerime yle koigi sonade ja kirjutame tulemusmassiivi need, mis vastavad tingimustele.
    for (int j = 0; j < n; ++ j) {
      // Kontrollime, kas sona indeksiga j vastab tingimustele.
      if (sonasKolmYhesugust [j]) {
        // Kui jah, siis kirjutame ta tulemusmassiivi esimesele vabale indeksile i ja siis suurendame i-d.
        tulemus [i] = sonad [j];
        ++ i;
      }
    }
    return tulemus;
  }
  // Teine, lihtsam implementatsioon - listiga.
  static String [] kolmYhesugustListiga (String s) {
    // Jagame lause sonadeks.
    String [] sonad = s . split (" ");
    // Kirjutame tulemused ArrayList-i.
    // See on lihtne lahendus olukorras, kui me ei tea ette, kui pikk tulemusmassiiv olema peab.
    List <String> tulemusList = new ArrayList <> ();
    // Itereerime yle lause sonade.
    for (int i = 0; i < sonad . length; ++ i) {
      // Kontrollime, kas leiame 3 yhesugust tahte jarjest.
      if (kolmYhesugust (sonad [i])) {
        // Kui on, siis lisame sona tulemuslisti.
        tulemusList . add (sonad [i]);
      }
    }
    // Teisendame tulemuse massiiviks.
    // Abimuutuja tulemusMassiiv on vajalik, sest kui me yritaks tagastada tulemusList . toArray (), siis juhtuks tyybiviga.
	String [] tulemusMassiiv = new String [tulemusList . size ()];
    return tulemusList . toArray (tulemusMassiiv);
  }
  static void test (String s) {
    System.out.println (s);
    System.out.println ("Massiiviga:");
    for (String t : kolmYhesugustMassiiviga (s)) {
      System.out.println (t);
    }
    System.out.println ("ArrayList-iga:");
    for (String t : kolmYhesugustListiga (s)) {
      System.out.println (t);
    }
  }
  public static void main (String [] args) {
    test ("Ansambli Jaaaar igaaastane masssalvestus Kuuuurija saates");
  }
}