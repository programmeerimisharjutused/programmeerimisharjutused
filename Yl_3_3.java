import java.util.Arrays;
class Yl_3_3 {
  // Leiab massiivi kaks suurimat arvu.
  static int [] max2 (int [] xs) {
    // tulemus [0] on suuruselt teine arv.
    // tulemus [1] on suurim arv.
    int [] tulemus = new int [2];
    // Alguses votame molema vaartusteks int-i minimaalse vaartuse.
    // See tagab, et iga arv, mis massiivist leitakse, voetakse tulemuse leidmisel arvesse.
    // Kui votaksime algvaartusteks nullid, juhtuks viga, sest massiiv voib koosneda ainult negatiivsetest elementidest.
    Arrays.fill (tulemus, Integer.MIN_VALUE);
    // Iga massiivi elemendi jaoks.
    for (int x : xs) {
      // Kui x on vaiksem voi vordne kui seni suuruselt teine element, ei ole tarvis midagi teha.
      // Kui x on suurem kui seni suuruselt teine element, siis tuleb suuruselt teine element (ja vb ka suurim) ymber kirjutada.
      if (x > tulemus [0]) {
        // Kui x on suurem kui seni suuruselt teine element, aga vaiksem kui suurim.
        if (x < tulemus [1]) {
          // Kirjutame suuruselt teise elemendi ymber.
          tulemus [0] = x;
        }
        // Kui x on suurem voi vordne seni suurimast elemendist.
        if (x >= tulemus [1]) {
          // Seni suurim element nihkub suuruselt teiseks.
          tulemus [0] = tulemus [1];
          // Uus suurim element on x.
          tulemus [1] = x;
        }
      }
    }
    return tulemus;
  }
  static void testMax2 (int [] x) {
    int [] tulemus = max2 (x);
    System.out.println
      ("Massiivi " + Arrays.toString (x) + " kaks suurimat elementi on " + tulemus [0] + " ja " + tulemus [1] + ".");
  }
  // Testime igaks juhuks nii massiiviga, kus kaks suurimat elementi on erinevad, kui massiiviga, kus nad on vordsed.
  public static void main (String [] args) {
    testMax2 (new int [] {1, 2, 5, 4, 1, 7, 3, 3, 7});
    testMax2 (new int [] {1, 2, 5, 4, 1, 7, 3, 3, 6});
  }
}