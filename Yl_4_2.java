class Yl_4_2 {
  public static void main (String [] args) {
    int [] [] x = new int [] [] {new int [] {1, 2, 3}, new int [] {4, 5, 6}, new int [] {7, 8, 9}};
    tryki (x);
    // Maatriks y on maatriks x, mille kaks esimest rida on vahetatud.
    int [] [] y = vahetaRead (0, 1, x);
    tryki (y);
    // Muudame maatriksit y ja kontrollime, ega maatriks x sellest ei muutunud.
    y [1] [1] = 0;
    tryki (x);
  }
  // Trykib maatriksi.
  static void tryki (int [] [] xss) {
    // Alustuseks newline, et maatrikseid yksteisest eraldada.
    System.out.print ("\n");
    // Trykime rea elementhaaval.
    for (int [] xs : xss) {
      // Iga rea elemendi jarele trykime tyhiku.
      for (int x : xs) {
        System.out.print (x + " ");
      }
      // Rea lopus newline.
      System.out.print ("\n");
    }
  }
  // See meetod tagastab maatriksi, mis on nagu x, ainult et read indeksiga i ja j on vahetatud.
  // Sisendi ridahaaval kopeerimine on vajalik selleks, et me ei muudaks algset maatriksit x.
  static int [] [] vahetaRead (int i, int j, int [] [] x) {
    // m on ridade arv.
    int m = x . length;
    // Tulemusmaatriksis on sama palju ridu kui sisendmaatriksis.
    int [] [] tulemus = new int [m] [];
    // Läbime koik read.
    for (int k = 0; k < m; ++ k) {
      // Kopeerime sisendmaatriksi k-nda rea tulemusmaatriksi k-ndaks reaks.
      tulemus [k] = x [k] . clone ();
    }
    // Vahetame tulemusmaatriksis i-nda ja j-nda rea.
    // Ajutist muutujat on vaja selleks, et hoida i-ndat rida samal ajal kui me j-nda rea i-nda rea asemele paneme.
    int [] ajutine = tulemus [i];
    tulemus [i] = tulemus [j];
    tulemus [j] = ajutine;
    return tulemus;
  }
}