class Yl_11_13 {
  static int alluvateArv1 (Tipp t) {
    if (t == null) {
      return 0;
    }
    t . x = alluvateArv1 (t . v) + alluvateArv1 (t . p);
    return 1;
  }
  static void alluvateArv (Tipp t) {
    alluvateArv1 (t);
  }
  static Tipp testTipp (Tipp v, Tipp p) {
    return new Tipp ("", v, p);
  }
  static void test (Tipp t) {
    System.out.println ("Algne puu:");
    System.out.println (Tipp . toString (t));
    alluvateArv (t);
    System.out.println ("Uus puu:");
    System.out.println (Tipp . toString (t));
  }
  public static void main (String [] args) {
    test (testTipp (testTipp (testTipp (null, null), null), testTipp (null, testTipp (null, null))));
  }
}