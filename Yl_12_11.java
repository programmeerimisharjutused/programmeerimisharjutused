class Yl_12_11 {
  static Tipp teeUusPuu (Tipp t) {
    if (t == null) {
      return null;
    }
    return
      new Tipp (
        t . info,
        t . v == null && t . p != null ? new Tipp (t . p) : teeUusPuu (t . v),
        t . v != null && t . p == null ? new Tipp (t . v) : teeUusPuu (t . p));
  }
  static void test (Tipp t) {
    System.out.println ("Algne puu: ");
    System.out.println (Tipp . toString (t));
    System.out.println ("Uus puu: ");
    System.out.println (Tipp . toString (teeUusPuu (t)));
  }
  public static void main (String [] args) {
    test (
      new Tipp (
        "J",
        new Tipp (
          "E",
          new Tipp ("B", new Tipp ("A", null, null), new Tipp ("D", new Tipp ("C", null, null), null)),
          new Tipp ("H", new Tipp ("G", new Tipp ("F", null, null), null), new Tipp ("I", null, null))),
        new Tipp ("K", null, new Tipp ("L", null, null))));
  }
}