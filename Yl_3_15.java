import java.util.Arrays;
class Yl_3_15 {
  // Summeerib koik elemendid, mis asuvad 3-ga jaguval indeksil (0, 3, 6, ...) ja jaguvad ise ka 3-ga.
  static int summa (int [] x) {
    int tulemus = 0;
    // Selle asemel, et itereerida 1 kaupa ja kontrollida indeksi 3-ga jaguvust, liidame igal iteratsioonil indeksile 3.
    for (int i = 0; i < x . length; i += 3) {
      // Kontrollime elemendi 3-ga jaguvust.
      // Kui element jagub 3-ga, siis liidame ta tulemusele.
      if (x [i] % 3 == 0) {
        tulemus += x [i];
      }
    }
    return tulemus;
  }
  static void testSumma (int [] x) {
    System.out.println ("Massiivi " + Arrays.toString (x) + " jaoks on tulemus " + summa (x) + ".");
  }
  // Elemendid 3-ga jaguvatel indeksitel on {1, 4, 3}.
  // Neist jagub 3-ga ainult 3.
  // Tulemus peaks olema 3.
  public static void main (String [] args) {
    testSumma (new int [] {1, 2, 5, 4, 1, 7, 3, 3, 7});
  }
}