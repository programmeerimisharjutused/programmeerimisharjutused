class k40 {
  
  // Palun asenda siin väärtus enda täisnimega
  static String nimi = "Liisi Kerik, Härmel Nestra";
  
  // Ül 1 lahendus
  // Vajadusel lisa abimeetodeid
  
  static int onNull (Tipp t) {
    return t == null ? 1 : 0;
  }
  
  static int ul1(Tipp t) {
    if (t == null) {
      return 0;
    }
    return ul1 (t . v) + (onNull (t . v) + onNull (t . p)) % 2 + ul1 (t . p);
  }
  
  // Ül 2 lahendus
  // Vajadusel lisa abimeetodeid
  
  static Tipp ul2(Tipp t, String str0, String str1) {
    if (t == null)
      return null;
    return new Tipp(str0 + t.info, ul2(t.v, str1, str0), ul2(t.p, str1, str0));
  }
  
}
