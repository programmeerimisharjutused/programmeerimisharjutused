class Yl_12_4 {
  static int vordseSuurusegaAlampuudegaTippusid (Tipp t) {
    if (t == null) {
      return 0;
    }
    int tulemusv = vordseSuurusegaAlampuudegaTippusid (t . v);
    int tulemusp = vordseSuurusegaAlampuudegaTippusid (t . p);
    int suurusv = t . v == null ? 0 : t . v . x;
    int suurusp = t . p == null ? 0 : t . p . x;
    t . x = suurusv + 1 + suurusp;
    return tulemusv + (suurusv == suurusp ? 1 : 0) + tulemusp;
  }
  static Tipp testTipp (Tipp v, Tipp p) {
    return new Tipp ("", v, p);
  }
  static void test (Tipp t) {
    System.out.println ("Puu:");
    System.out.println (Tipp . toString (t));
    System.out.println ("Vordse suurusega alampuudega tippusid: " + vordseSuurusegaAlampuudegaTippusid (t));
  }
  public static void main (String [] args) {
    test (
      testTipp (
        testTipp (
          testTipp (testTipp (null, null), testTipp (testTipp (null, null), null)),
          testTipp (testTipp (testTipp (null, null), null), testTipp (null, null))),
        testTipp (null, testTipp (null, null))));
  }
}