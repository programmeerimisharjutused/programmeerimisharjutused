class Yl_10_14 {
  // Abifunktsioon, mis koostab sulgavaldisi.
  // tulemus on seni koostatud sulgavaldise algus.
  // n on sulgude arv, mis tuleb veel avada.
  // bilanss ytleb, mitu sulgu on hetkel veel kinni panemata.
  static void sulgavaldis1 (String tulemus, int n, int bilanss) {
    // Rekursioon lopeb, kui on avatud ja ka suletud vajalik arv sulgusid. Siis trykime tulemuse.
    if (n == 0 && bilanss == 0) {
      System.out.println (tulemus);
    }
    // Kui tuleb veel sulgusid avada, voime minna rekursiooni harusse, kus lisame avava sulu.
    // Sellisel juhul vaheneb avada jaanud sulgude arv yhe vorra.
    // Hetkel avatud sulgude arv suureneb yhe vorra.
    if (n > 0) {
      sulgavaldis1 (tulemus + "(", n - 1, bilanss + 1);
    }
    // Kui meil on hetkel vahemalt yks avatud sulg, voime minna rekursiooni harusse, kus lisame sulgeva sulu.
    // Sellisel juhul ei muutu avada jaanud sulgude arv.
    // Hetkel avatud sulgude arv suureneb yhe vorra.
    if (bilanss > 0) {
      sulgavaldis1 (tulemus + ")", n, bilanss - 1);
    }
    // NB! Need kaks if-tingimust ei valista yksteist!
    // Me voime minna kas esimesse, teise, mitte kumbagi (kui n == 0 && bilanss == 0) voi molemasse rekursiooniharusse.
    // See oleneb sellest, kas tohime parajasti sulgu ainult avada, ainult sulgeda, mitte kumbagi voi molemat.
  }
  // Koostab korrektseid sulgavaldisi pikkusega 2n.
  static void sulgavaldis (int n) {
    // Alguses on jaanud avada n sulgu ja bilanss on 0.
    sulgavaldis1 ("", n, 0);
  }
  public static void main (String [] args) {
    for (int i = 1; i <= 4; ++ i) {
      sulgavaldis (i);
    }
  }
}