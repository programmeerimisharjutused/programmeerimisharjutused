class Yl_5_12 {
  // Jatame stringi koigist korduvatest tahtedest alles ainult esimese vastava tahe.
  // Naiteks "hello world" -> "helo wrd". Eemaldame teise ja kolmanda l ja teise o.
  static String eemalda (String s) {
    // StringBuilder on vajalik, sest tavalist stringi ei saa muuta.
    // Teeme alguses tyhja StringBuilder-i. Hakkame sinna ükshaaval vajalikke tahti sisestama.
    StringBuilder tulemus = new StringBuilder ();
    // Labime jarjest koik algse stringi margid.
    for (int i = 0; i < s . length (); ++ i) {
      char c = s . charAt (i);
      // s . indexOf (c) tagastab -1, kui stringis s ei leidu tahte c, ja esimese asukoha indeksi, kui leidub.
      // Kui s . indexOf (c) < i, siis see tahendab, et tahte c ei leidu (voimatu) voi see esineb juba varem.
      // Kui c esineb stringis juba varem, siis me seda tulemusse ei lisa.
      // Kui s . indexOf (c) == i, siis see tahendab, et antud kohas esineb c esimest korda.
      // Sellisel juhul lisame selle tulemusse.
      // s . indexOf (c) > i on voimatu, sest indeksil i on juba taht c.
      if (s . indexOf (c) == i) {
        tulemus . append (c);
      }
    }
    return tulemus . toString ();
  }
  static void test (String s) {
    System.out.println (s + " -> " + eemalda (s));
  }
  public static void main (String [] args) {
    test ("hello world");
    test ("stringbuilder");
  }
}