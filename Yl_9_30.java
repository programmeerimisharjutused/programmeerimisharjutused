import java.util.Arrays;
class Yl_9_30 {
  // Abifunktsoon. Lisaks lubatud jupi-pikkustele ja traadi allesjaanud pikkusele saab argumendiks ka seni kogutud tulemuse.
  static void traadijupid1 (int [] pikkused, int [] tulemus, int traat) {
    // Tuvastame, kas traati saab veel loigata. Alguses eeldame, et ei saa.
    boolean saabLoigata = false;
    // Kontrollime iga lubatud jupi-pikkuse jaoks, kas seda on veel voimalik traadist loigata.
    for (int pikkus : pikkused) {
      if (traat >= pikkus) {
        // Kui jah, siis katkestame tsykli - saime teada, et traati saab edasi loigata.
        saabLoigata = true;
        break;
      }
    }
    // Kui traati saab edasi loigata.
    if (saabLoigata) {
      // Iga jupi-pikkuse jaoks, kui seda saab veel traadist loigata, teeme rekursiivse kutse.
      for (int pikkus : pikkused) {
        if (traat >= pikkus) {
          // Kopeerime tulemusmassiivi suurendades selle pikkust yhe vorra.
          int [] tulemus1 = new int [tulemus . length + 1];
          for (int i = 0; i < tulemus . length; ++ i) {
            tulemus1 [i] = tulemus [i];
          }
          // Uue tulemusmassiivi viimane element on uue loigatud traadijupi pikkus.
          tulemus1 [tulemus . length] = pikkus;
          // Traadi allesjaanud pikkus vaheneb loigatud pikkuse vorra.
          traadijupid1 (pikkused, tulemus1, traat - pikkus);
        }
      }
    // Kui traati ei saa edasi loigata, siis trykime tekitatud jupid, ylejaagi ja juppide arvu.
    } else {
      System.out.println (Arrays.toString (tulemus) + ", alles " + traat + ", " + tulemus . length + " juppi");
    }
  }
  // Funktsioon saab lubatud juppide pikkused ja traadi pikkuse ning naitab koik voimalused traati juppideks loigata.
  static void traadijupid (int [] pikkused, int traat) {
    // Alguses on tulemusmassiiv (teine argument) tyhi.
    System.out.println ("Traat pikkusega " + traat + ", lubatud jupid " + Arrays.toString (pikkused));
    traadijupid1 (pikkused, new int [] {}, traat);
  }
  public static void main (String [] args) {
    traadijupid (new int [] {2}, 3);
    traadijupid (new int [] {2, 3}, 5);
    traadijupid (new int [] {2, 3, 5}, 7);
  }
}